// console.log('Hello World');

/*
    -Javascript has built-in function and methods for arrays. This allows us to manipulate and access array items.
    -Array can be either mutated or iterated
        - Array "mutations" seek to modify the contents of an array while array "iteration" aim to evaluate and loop over each element.
*/

// Mutator Methods
// functions / methods that mutate or change an array
// These manipulates the original array performing task such as adding and removing elements

console.log('== Mutator Methods ==')
console.log('---------------------------------------');

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

// push()
/* 
    - adds an element in the end of an array AND returns the new array's length.
    - Syntax:
        arrayName.push[newElement];
*/

console.log('=>push()');
console.log('fruits array: ');
console.log(fruits);

// fruits[fruits.length] = 'Mango';
let fruitsLength = fruits.push('Mango');
console.log('Size / length of fruits array: ' + fruits.length);
console.log('Mutated array from push("Mango"): ');
console.log(fruits)

fruits.push('Avocado', 'Guava');
// console.log(fruits.push('Avocado', 'Guava')); /* returns the new array's length */
console.log('Mutated array from push("Avocado", "Guava"): ');
console.log(fruits);

// function addMultipleFruits(fruit1, fruit2, fruit3){
//     fruits.push(fruit1, fruit2, fruit3);
//     console.log(fruits);
// }

// addMultipleFruits('Durian', 'Atis');


console.log('---------------------------------------');


// pop()
/* 
    - removes the last element in an array AND returns the removed element
*/

console.log('==>pop()');

let removedFruit = fruits.pop();
console.log(removedFruit); /* returns the removed fruit */
console.log('Mutated array from pop method: ');
console.log(fruits);


console.log('---------------------------------------');


// Unshift
/*
    - Add one or more elements at the beginning of an array.
    - Syntax:
        arrayName.unshift("elementA");
        arrayName.unshift("elementA", "elementB");
*/

console.log('==>unshift()');

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift ("Lime", "Banana"): ');
console.log(fruits);


console.log('---------------------------------------');

// shift()
/* 
    - removes an element at the beginning of an array AND returns the removed statement
*/

console.log('==>shift()');
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method: ');
console.log(fruits);


console.log('---------------------------------------');


// splice()
/*
    - simulatanously removes an element from a specified index number and adds new elements.
    - Syntax:
        arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

console.log('==>splice()');

// indeces     0         1         2        3           4           5         6
        // ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']


           // s  d    elements to be added
fruits.splice(1, 2, 'Lime', 'Cherry', 'Tomato'); /* the elements to be added are added through replacing from starting index */
console.log('Mutated array from splice method: ');
console.log(fruits);

// We could also use splice to remove an elemet/s to do that our syntax should be:
/* 
    arrayName.splice(startingIndex, deleteCount);
*/


console.log('---------------------------------------');


// sort()
/*
    -Rearranges the array elements in alphanumeric order
    -Syntax:
        - arrayName.sort();
*/

console.log('==>sort()');
fruits.sort();
console.log('Mutated array from sort method: ');
console.log(fruits);


console.log('---------------------------------------');


// reverse()
/* 
    - Reverse the order of array elements
    - Syntax:
        arrayName.reverse();
*/

console.log('==>reverse()');
fruits.reverse();
console.log('Mutated array from reverse method: ');
console.log(fruits);


console.log('---------------------------------------');


// Non-mutator
/* 
    - Non-mutator methods are functions that do not modify or change an array after they're created.
*/


console.log("== Mutator Methods ==");


                // 0    1      2     3     4     5     6     7
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];


// indexOf()
/*
    - Returns the index of the first matching element found in an array.
    - If no match was found, the result will be -1.
    - The search process will bne done from the first element proceeding to the last element.
    - Syntax:
        arrayName.indexOf(searchValue);
        arrayName.indexOf(searchValue, fromIndex/startingIndex);
*/

console.log('=>indexOf()');
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf("PH"): ' + firstIndex);

                                // 'PH' - element to add
                                // 2 - starting index where to search
// let firstIndex = countries.indexOf('PH', 2); /* with a specified index to start */
// console.log(firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf("BR"): ' + invalidCountry);


console.log('---------------------------------------');


// lastIndexOf()
/*
    - Returns the index number of the last matching element found in an array.
    - The search from process will be done from the last element proceeding to the first element.
    - Syntax:
        arrayName.lastIndexOf(searchValue);
        arrayName.lastIndexOF(searchValue, fromIndex/EndingIndex);
*/

console.log("=>lastIndexOf()");
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf('PH'): " +lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log('Result of indexOf("PH", 4): ' + lastIndexStart);


console.log('---------------------------------------');


// slice()
/*
    - Portions/slices element from an array AND returns a new array.
    - Syntax:
        arrayName.slice(startingIndex); //until the last element of the array.
        arrayName.slice(startingIndex, endingIndex);
*/

console.log("=>slice()");

console.log("Original countries array:");
console.log(countries);

// Slicing off elements from specified index to the last element

let sliceArrayA = countries.slice(2);

// 0    1      2     3     4     5     6     7
// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log("Result from slice(2): ");
console.log(sliceArrayA);

// Slicing off element from specified index to another index. But the specified last index is not included in the return.
let sliceArrayB = countries.slice(2, 5); //2 -> 4 // ends with the last declared index minus one
console.log("Result from slice(2, 5): ");
console.log(sliceArrayB);

// Slicing off elements starting from the last element of an array
//  8-     7-     6-     5-    4-   3-    2-     1-
// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let sliceArrayC = countries.slice(-3);
    console.log("Result from slice method:");
    console.log(sliceArrayC);


console.log('---------------------------------------');


// toString
/*
    - Returns an array as a string, separated by commas.
    - Syntax 
        arrayName.toString();
*/

console.log("=>toString()");

// ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
// console.log(stringArray[0]);
console.log(typeof stringArray); //to check if the array is converted to string.